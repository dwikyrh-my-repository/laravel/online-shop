<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'preventBackHistory']);
    }

    # get all data categories
    public function index()
    {
        # action get all data categories
        $categories = Category::latest()->when(request()->q, function ($categories) {
            $categories = $categories->where('name', 'like', '%' . request()->q . '%');
        })->paginate(15);

        # return view
        return view('admin.categories.index', compact('categories'));
    }

    # view create category
    public function create()
    {
        $category = new Category();
        return view('admin.categories.create', compact('category'));
    }

    # process create category
    public function store(Request $request)
    {
        # validation
        $request->validate([
            'image' => ['required', 'image', 'mimes:jpg,jpeg,png', 'max:2048'],
            'name' => ['required', 'min:3', 'max:150', Rule::unique('categories', 'name')],
        ]);

        # upload image handle
        $image = $request->file('image');
        $image->storeAs('categories/img', $image->hashName());

        # action create category
        Category::create([
            'image' => $image->hashName(),
            'name' => $request->name,
            'slug' => str($request->name)->slug()
        ]);

        # redirect
        return to_route('admin.categories.index')->with('success', 'Data category has been created');
    }

    public function show(Category $category)
    {
    }

    # view edit category
    public function edit(Category $category)
    {
        return view('admin.categories.edit', compact('category'));
    }

    # process update category
    public function update(Request $request, Category $category)
    {
        # validation
        $request->validate([
            'image' => ['nullable', 'image', 'mimes:jpg,jpeg,png', 'max:2048'],
            'name' => ['required', 'min:3', 'max:150', Rule::unique('categories', 'name')->ignore($category)],
        ]);

        # action update category
        if($request->file('image') == '')
        {
            $category->update([
                'name' => $request->name,
                'slug' => str($request->name)->slug()
            ]);
        } else {
            $image = $request->file('image');
            Storage::disk('public')->delete('categories/img/' . basename($category->image));
            $image->storeAs('categories/img', $image->hashName());

            $category->update([
                'image' => $image->hashName(),
                'name' => $request->name,
                'slug' => str($request->name)->slug()
            ]);
        }

        # redirect
        return to_route('admin.categories.index')->with('success', 'Data category has been updated');
    }

    # process delete category
    public function destroy(Category $category)
    {
        # delete category image
        Storage::disk('public')->delete('categories/img/' . basename($category->image));

        # action delete category
        $category = $category->delete();

        if ($category) {
            return response()->json([
                'status' => 'success'
            ]);
        }
    }
}
