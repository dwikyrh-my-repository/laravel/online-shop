<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'preventBackHistory']);
    }

    # get all data customers
    public function index()
    {
        # action get all data customers
        $customers = Customer::latest()->when(request()->q, function($customers) {
            $customers = $customers->where('name', 'like', '%'. request()->q . '%');
        })->paginate(15);

        # return view
        return view('admin.customers.index', compact('customers'));
    }
}