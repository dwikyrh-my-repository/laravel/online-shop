<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\Order;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'preventBackHistory']);
    }

    # get all data Invoice
    public function index()
    {
        # action get all data invoices
        $invoices = Invoice::latest()->when(request()->q, function($invoices) {
            $invoices = $invoices->where('invoice', 'like', '%' . request()->q . '%');
        })->paginate(15);

        # return view
        return view('admin.orders.index', compact('invoices'));
    }

    # get spesific invoice
    public function show(Invoice $invoice)
    {
        # return view
        return view('admin.orders.show', compact('invoice'));
    }
}
