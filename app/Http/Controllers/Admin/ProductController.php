<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'preventBackHistory']);
    }

    # get all data products
    public function index()
    {
        # action get all data products
        $products = Product::latest()->when(request()->q, function ($products) {
            $products = $products->where('title', 'like', '%' . request()->q . '%');
        })->paginate(15);

        # return view
        return view('admin.products.index', compact('products'));
    }

    # view create product
    public function create()
    {
        # get data categories
        $categories = Category::select('id', 'name')->get();
        $product = new Product();

        # return view
        return view('admin.products.create', compact('categories', 'product'));
    }

    # process create product
    public function store(Request $request)
    {
        # validation
        $request->validate([
            'image' => ['required', 'image', 'mimes:jpg,jpeg,png', 'max:2048'],
            'title' => ['required', 'min: 3', 'max: 255', Rule::unique('products', 'title')],
            'category' => ['required'],
            'content' => ['required', 'min: 3'],
            'weight' => ['required', 'integer'],
            'price' => ['required', 'integer'],
            'discount' => ['required', 'integer'],
        ]);

        # upload image handle
        $image = $request->file('image');
        $image->storeAs('products/img', $image->hashName());

        # action create product
        Product::create([
            'image' => $image->hashName(),
            'title' => $request->title,
            'slug' => str($request->title)->slug(),
            'category_id' => $request->category,
            'content' => $request->content,
            'unit' => $request->unit,
            'weight' => $request->weight,
            'price' => $request->price,
            'discount' => $request->discount,
            'keywords' => $request->keywords,
            'description' => $request->description,
        ]);

        # redirect
        return to_route('admin.products.index')->with('success', 'Data product has been created');
    }

    public function show(Product $product)
    {
        //
    }

    # view edit product
    public function edit(Product $product)
    {
        # get data category
        $categories = Category::select('id', 'name')->get();

        # return view
        return view('admin.products.edit', compact('categories', 'product'));
    }

    # process update product
    public function update(Request $request, Product $product)
    {
        # validation
        $request->validate([
            'image' => ['nullable', 'image', 'mimes:jpg,jpeg,png', 'max:2048'],
            'title' => ['required', 'min:3', 'max:255', Rule::unique('products', 'title')->ignore($product)],
            'category' => ['required'],
            'content' => ['required', 'min: 3'],
            'weight' => ['required', 'integer'],
            'price' => ['required', 'integer'],
            'discount' => ['required', 'integer'],
        ]);

        # update image handle
        if ($request->hasFile('image')) {
            if ($product->image) {
                Storage::delete($product->image);
            }
            $image = $request->file('image')->store('products/img');
        } else {
            $image = $product->image;
        }

        # action update product
        if ($request->file('image') == '') {
            $product->update([
                'title' => $request->title,
                'slug' => str($request->title)->slug(),
                'category_id' => $request->category,
                'content' => $request->content,
                'unit' => $request->unit,
                'weight' => $request->weight,
                'price' => $request->price,
                'discount' => $request->discount,
                'keywords' => $request->keywords,
                'description' => $request->description,
            ]);
        } else {
            $image = $request->file('image');
            Storage::disk('public')->delete('products/img/' . basename($product->image));
            $image->storeAs('products/img', $image->hashName());

            $product->update([
                'image' => $image->hashName(),
                'title' => $request->title,
                'slug' => str($request->title)->slug(),
                'category_id' => $request->category,
                'content' => $request->content,
                'unit' => $request->unit,
                'weight' => $request->weight,
                'price' => $request->price,
                'discount' => $request->discount,
                'keywords' => $request->keywords,
                'description' => $request->description,
            ]);
        }

        # redirect
        return to_route('admin.products.index')->with('success', 'Data product has been updated');
    }

    public function destroy(Product $product)
    {
        # delete product image
        Storage::disk('public')->delete('products/img/' . basename($product->image));
        # action delete product
        $product = $product->delete();

        if ($product) {
            return response()->json([
                'status' => 'success'
            ]);
        }
    }
}
