<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'preventBackHistory']);
    }

    # view profile
    public function index()
    {
        # retur view
        return view('admin.profile.index');
    }
}
