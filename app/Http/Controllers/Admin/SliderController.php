<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SliderController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'preventBackHistory']);
    }

    # view all data slider
    public function index()
    {
        # action all data sliders
        $sliders = Slider::latest()->paginate(5);
        # return view
        return view('admin.sliders.index', compact('sliders'));
    }

    # process create slider
    public function store(Request $request)
    {
        # validation
        $request->validate([
            'image' => ['required', 'image', 'mimes:jpg,jpeg,png', 'max:2048'],
            'link'  => ['required', 'min:3', 'max:255'],
        ]);

        # upload image handle
        $image = $request->file('image');
        $image->storeAs('sliders/img', $image->hashName());

        # action create slider
        Slider::create([
            'image' => $image->hashName(),
            'link' => $request->link,
        ]);

        # redirect
        return to_route('admin.sliders.index')->with('success', 'Data slider has been created');
    }

    public function destroy(Slider $slider)
    {
        # delete category image
        Storage::disk('public')->delete('sliders/img/' . basename($slider->image));

        # action delete slider
        $slider = $slider->delete();

        if ($slider) {
            return response()->json([
                'status' => 'success'
            ]);
        }
    }
}
