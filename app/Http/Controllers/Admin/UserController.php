<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'preventBackHistory']);
    }

    # get all data users
    public function index()
    {
        # action get all data users
        $users = User::latest()->when(request()->q, function ($users) {
            $users = $users->where('name', 'like', '%' . request()->q . '%');
        })->whereNot('id', auth()->user()->id)->paginate(15);

        # return view
        return view('admin.users.index', compact('users'));
    }

    # view create user
    public function create()
    {
        $user = new User();
        return view('admin.users.create', compact('user'));
    }

    # process create user
    public function store(Request $request)
    {
        # validation
        $request->validate([
            'name' => ['required', 'min:3', 'max:255'],
            'email' => ['required', 'email', Rule::unique('users', 'name')],
            'password' => ['required', 'min:3', 'max:255', 'confirmed'],
        ]);

        # action create user
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        # redirect
        return to_route('admin.users.index')->with('success', 'Data user has been created');
    }

    public function show(User $user)
    {

    }

    # view edit user
    public function edit(User $user)
    {
        return view('admin.users.edit', compact('user'));
    }

    # process update user
    public function update(Request $request, User $user)
    {
        # validation
        $request->validate([
            'name' => ['required', 'min:3', 'max:255'],
            'email' => ['required', 'email', Rule::unique('users', 'name')->ignore($user)],
            'password' => ['nullable', 'min:8', 'max:255', 'confirmed'],
        ]);

        # action update user
        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        # redirect
        return to_route('admin.users.index')->with('success', 'Data user has been updated');
    }

    # process delete user
    public function destroy(User $user)
    {
        # action delete user
        $user->delete();

        return response()->json([
            'status' => 'success',
        ]);
    }
}