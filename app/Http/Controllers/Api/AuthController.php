<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use PHPOpenSourceSaver\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    # method __construct for middleware
    public function __construct()
    {
        $this->middleware('auth:api')->except('register', 'login');
    }

    # process register user
    public function register(Request $request)
    {
        # validation
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'min:3', 'max:255', 'string'],
            'email' => ['required', 'email', Rule::unique('customers', 'email')],
            'password' => ['required', 'confirmed', 'min:8'],
        ]);

        # if validation errors
        if ($validator->fails())
        {
            return response()->json($validator->errors(), 400);
        }

        # action register user
        $customer = Customer::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        # generate token
        $token = JWTAuth::fromUser($customer);

        # if register successfully
        if($customer)
        {
            return response()->json([
                'success' => true,
                'user' => $customer,
                'token' => $token
            ], 201);
        }

        # if register failed
        return response()->json([
            'success' => false,
        ], 409);
    }

    # process login user
    public function login(Request $request)
    {
        # validation
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        # if validation errors
        if($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        # credentials
        $creadentials = $request->only('email', 'password');

        # if failed login
        if(!$token = auth()->guard('api')->attempt($creadentials)) {
            return response()->json([
                'success' => false,
                'message' => 'Email or Password is incorrect',
            ], 401);
        }

        # if success login
        return response()->json([
            'success' => true,
            'user' => auth()->guard('api')->user(),
            'token' => $token,
        ]);
    }


    # get user
    public function getUser()
    {
        return response()->json([
            'success' => true,
            'user' => auth()->user()
        ], 200);
    }
}