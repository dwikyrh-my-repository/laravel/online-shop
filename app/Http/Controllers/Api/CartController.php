<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
    # method __construct for middleware
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    # get product in cart
    public function index()
    {
        # action get product in cart
        $carts = Cart::with('product')->where('customer_id', auth()->guard('api')->user()->id)->latest()->get();

        # return response
        return response()->json([
            'success' => true,
            'message' => 'List Data Cart',
            'cart' => $carts
        ]);
    }

    # add to cart
    public function store(Request $request)
    {
        $item = Cart::where('product_id', $request->product_id)->where('customer_id', $request->customer_id);

        if ($item->count())
        {
            # increment quantity
            $item->increment('quantity');
            $item = $item->first();

            # sum price * quantity
            $price = $request->price * $item->quantity;

            # sum weight
            $weight = $request->weight * $item->quantity;

            $item->update([
                'price' => $price,
                'weight' => $weight,
            ]);
        } else {
            $item = Cart::create([
                'product_id' => $request->product_id,
                'customer_id' => $request->customer_id,
                'quantity' => $request->quantity,
                'price' => $request->price,
                'weight' => $request->weight
            ]);
        }

        # return response
        return response()->json([
            'success' => true,
            'message' => 'Success Add To Cart',
            'quantity' => $item->quantity,
            'product' => $item->product,
        ]);
    }

    # get total price
    public function getTotalCart()
    {
        # action get total price
        $carts = Cart::with('product')->where('customer_id', auth()->guard('api')->user()->id)
        ->orderByDesc('created_at')->sum('price');

        # return response
        return response()->json([
            'success' => true,
            'message' => 'Total Cart Price ',
            'total' => $carts,
        ]);
    }

    # get total weight
    public function getTotalWeight()
    {
        # action get total weight
        $carts = Cart::with('product')->where('customer_id', auth()->guard('api')->user()->id)
        ->orderByDesc('created_at')->sum('weight');

        # return response
        return response()->json([
            'success' => true,
            'message' => 'Total Cart Weight ',
            'total' => $carts,
        ]);
    }

    # remove cart
    public function removeCart(Request $request)
    {
        # action remove cart
        Cart::with('product')->where('id', $request->cart_id)->delete();

        # return response
        return response()->json([
            'success' => true,
            'message' => 'Remove Item Cart'
        ]);
    }

    # remove all cart (if checout success)
    public function removeAllCart(Request $request)
    {
        # action remove all cart
        Cart::with('product')->where('customer_id', auth()->guard('api')->user()->id)->delete();
        # return response
        return response()->json([
            'success' => true,
            'message' => 'Remove All Item in Cart',
        ]);
    }
}