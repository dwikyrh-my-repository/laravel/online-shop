<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    # get all data categories
    public function index()
    {
        # action get all data categories
        $categories = Category::latest()->get();

        # return response
        return response()->json([
            'success' => true,
            'message' => 'List Data Categories',
            'categories' => $categories,
        ]);
    }

    # get 5 data categories
    public function getFiveCategories()
    {
        # action get 5 data categories
        $categories = Category::latest()->take(5)->get();

        # return response
        return response()->json([
            'success' => true,
            'message' => 'List Data Categoresi',
            'categories' => $categories,
        ], 200);
    }

    # get product by category
    public function show(Category $category)
    {
        if ($category) {
            # if success get data category
            return response()->json([
                'success' => true,
                'message' => 'List Product by Category: ' . $category->name,
                'product' => $category->products()->latest()->get(),
            ], 200);
        } else {
            # if failed get data category
            return response()->json([
                'success' => false,
                'message' => 'Data Product By Category Not Found',
            ], 404);
        }
    }

    # get some categories for header
    public function categoryHeader()
    {
        # get 5 categories for header
        $categories = Category::latest()->take(5)->get();

        # return response
        return response()->json([
            'success' => true,
            'message' => 'List Data Category Header',
            'categories' => $categories,
        ]);
    }
}
