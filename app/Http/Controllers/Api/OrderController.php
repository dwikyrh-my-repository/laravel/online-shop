<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    # method __construct for middleware
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    # get all data invoices
    public function index()
    {
        # action get all data invoices
        $invoices = Invoice::where('customer_id', auth()->guard('api')->user()->id)->latest()->get();

        # return response
        return response()->json([
            'success' => true,
            'message' => 'List Invoices: ' . auth()->guard('api')->user()->name,
            'data' => $invoices,
        ], 200);
    }

    #  get data invoice spesific
    public function show($snap_token)
    {
        # process get data invoice spesific
        $invoice = Invoice::with('provinces', 'cities')->where('customer_id', auth()->guard('api')->user()->id)->where('snap_token', $snap_token)->latest()->first();

        # return response
        return response()->json([
            'success' => true,
            'message' => 'Detail Invoices: ' . auth()->guard('api')->user()->name,
            'data' => $invoice,
            'product' => $invoice->orders,
        ], 200);
   }
}
