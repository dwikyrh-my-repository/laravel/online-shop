<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    # get all data products
    public function index(Request $request)
    {
        # action get all data products
        $products = Product::latest()->get();

        # return response
        return response()->json([
            'success' => true,
            'message' => 'List Data Products',
            'products' => $products,
        ], 200);
    }

    # get product details
    public function show(Product $product)
    {
        if ($product) {
            # if product success to show
            return response()->json([
                'success' => true,
                'message' => 'Detail Data Product' . $product->name,
                'product' => $product,
            ], 200);
        } else {
            # if product failed to show
            return response()->json([
                'success' => false,
                'message' => 'Data Product Not Found',
            ], 404);
        }
    }
}
