<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class RajaOngkirController extends Controller
{
    # get all data provinces
    public function getProvinces()
    {
        # action get all data provinces
        $provinces = Province::all();

        # return response
        return response()->json([
            'success' => true,
            'message' => 'List Data Provinces',
            'data' => $provinces,
        ]);
    }

    # get all data cities
    public function getCities(Request $request)
    {
        # get data cities by provinces
        $city = City::where('province_id', $request->province_id)->get();
        return response()->json([
            'success' => true,
            'message' => 'List Data Cities By Provinces',
            'data' => $city,
        ]);
    }

    # check ongkir
    public function checkOngkir(Request $request)
    {
        # fetch rest api
        $response = Http::withHeaders([
            # api key rajaongkir
            'key' => config('services.rajaongkir.key')
        ])->post('https://api.rajaongkir.com/starter/cost', [
            # send data
            'origin' => 23, # Id Bandung
            'destination' => $request->city_destination,
            'weight' => $request->weight,
            'courier' => $request->courier,
        ]);

        # return response
        return response()->json([
            'success' => true,
            'message' => 'List Data Cost All Courier: ' . $request->courier,
            'data' => $response['rajaongkir']['results'][0]
        ], 200);
    }
}