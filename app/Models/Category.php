<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    # fillable
    protected $fillable = [
        'name', 'slug', 'image',
    ];

    # relation table
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    # route key name
    public function getRouteKeyName()
    {
        return 'slug';
    }


    protected function image(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => asset('/storage/categories/img/' . $value),
        );
    }
}