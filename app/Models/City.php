<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;

    # fillable
    protected $fillable = [
        'province_id', 'city_id', 'name',
    ];

    # relation table
    public function invoice()
    {
        return $this->hasOne(Invoice::class, 'city', 'id');
    }
}