<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;

    # fillable
    protected $fillable = [
        'invoice', 'customer_id', 'courier', 'service', 'cost_courier', 'weight', 'name', 'phone', 'province',
        'city', 'address', 'status', 'snap_token', 'grand_total'
    ];

    # relation table
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function provinces()
    {
        return $this->belongsTo(Province::class, 'province', 'id');
    }

    public function cities()
    {
        return $this->belongsTo(City::class, 'city', 'id');
    }
}
