<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    # fillable
    protected $fillable = [
        'image', 'title', 'slug', 'category_id', 'content', 'weight', 'height', 'price', 'discount',
    ];

    # with
    protected $with = [
        'category'
    ];
    # relation table
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function carts()
    {
        return $this->hasMany(Cart::class);
    }

    # route key name
    public function getRouteKeyName()
    {
        return 'slug';
    }

    # attribute
    protected function image(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => asset('/storage/products/img/' . $value),
        );
    }
}