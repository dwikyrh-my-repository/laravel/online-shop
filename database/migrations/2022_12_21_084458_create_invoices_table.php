<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->text('invoice');
            $table->foreignId('customer_id');
            $table->string('courier', 255);
            $table->string('service', 255);
            $table->bigInteger('cost_courier');
            $table->bigInteger('weight');
            $table->string('name', 255);
            $table->bigInteger('phone');
            $table->foreignId('province');
            $table->foreignId('city');
            $table->text('address');
            $table->enum('status', ['pending', 'success', 'failed', 'expired']);
            $table->string('snap_token', 255)->nullable();
            $table->bigInteger('grand_total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
};