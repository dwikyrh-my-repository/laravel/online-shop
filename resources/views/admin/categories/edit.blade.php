@extends('layouts.app', ['title' => 'Edit Category - GreenK Online Shop'])
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card border-0 shadow">
        {{-- card header --}}
        <div class="card-header">
          <h6 class="m-0 font-weight-bold"><i class="fas fa-folder mr-2"></i> Edit Category</h6>
        </div>
        {{-- card body --}}
        <div class="card-body">
          {{-- form create category --}}
          <form action="{{ route('admin.categories.update', $category) }}" method="POST" enctype="multipart/form-data">
            @method('PUT')
            @include('admin.categories.form', ['submit' => 'Update'])
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
