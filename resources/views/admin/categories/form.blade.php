  @csrf
  {{-- image --}}
  <div class="form-group">
    <label for="image" class="form-label">Image</label>
    <input type="file" name="image" class="form-control @error('image') is-invalid @enderror" id="image" accept="image/*">
    @error('image')
    <div class="invalid-feedback">
      <strong>{{ $message }}</strong>
    </div>
    @enderror
  </div>

  {{-- category name --}}
  <div class="form-group">
    <label for="name" class="form-label">Category Name</label>
    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name" value="{{ old('name', $category->name) }}"
    placeholder="Category Name">
    @error('name')
    <div class="invalid-feedback">
      <strong>{{ $message }}</strong>
    </div>
    @enderror
  </div>

  {{-- button submit --}}
  <button type="submit" class="btn btn-primary mr-1 btn-submit"> {{ $submit }}</button>
