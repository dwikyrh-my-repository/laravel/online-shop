@extends('layouts.app', ['title' => 'Customers - GreenK Online Shop'])

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card border-0 shadow">
        {{-- card header --}}
        <div class="card-header">
          <h6 class="m-0 font-weight-bold"><i class="fas fa-users mr-2"></i> Customers</h6>
        </div>
        {{-- card body --}}
        <div class="card-body">
          {{-- search customers --}}
          <form action="{{ route('admin.customers.index') }}" method="GET">
            <div class="row justify-content-end w-100">
              <div class="col-md-8">
                <div class="form-group">
                  <div class="input-group">
                    <input type="search" class="form-control form-control-md" name="q" placeholder="Search Customers...">
                    <div class="input-group-append">
                      <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th scope="col" style="text-align: center;width: 6%">NO.</th>
                  <th scope="col">Customer Name</th>
                  <th scope="col">Email</th>
                  <th scope="col">Join</th>
                </tr>
              </thead>
              <tbody>
                @forelse ($customers as $customer)
                <tr>
                  <td scope="row" style="text-align: center">{{ $customers->firstItem() + $loop->index }}</td>
                  <td scope="row">{{ $customer->name }}</td>
                  <td scope="row">{{ $customer->email }}</td>
                  <td scope="row">{{ dateID($customer->created_at) }}</td>
                </tr>
                @empty
                <tr>
                  <td scope="row" colspan="4">
                    <div class="text-danger text-center">No data customers.</div>
                  </td>
                </tr>
                @endforelse
              </tbody>
            </table>
            {{-- pagination --}}
            <div style="text-align: center">
              {{ $customers->links() }}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
