@extends('layouts.app', ['title' => 'Orders - GreenK Online Shop'])
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card border-0 shadow">
        {{-- card header --}}
        <div class="card-header">
          <h6 class="m-0 font-weight-bold"><i class="fas fa-shopping-cart mr-2"></i> Orders</h6>
        </div>
        {{-- card body --}}
        <div class="card-body">
          {{-- search orders --}}
          <form action="{{ route('admin.orders.index') }}" method="GET">
            <div class="row justify-content-end w-100">
              <div class="col-md-8">
                <div class="form-group">
                  <div class="input-group">
                    <input type="search" class="form-control form-control-md" name="q" placeholder="Search Orders...">
                    <div class="input-group-append">
                      <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
          {{-- table of orders  --}}
          <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th scope="col" style="text-align: center;width: 6%">No.</th>
                  <th scope="col">No. Invoice</th>
                  <th scope="col">Full Name</th>
                  <th scope="col">Grand Total</th>
                  <th scope="col">Status</th>
                  <th scope="col" style="width: 15%;text-align: center">Action</th>
                </tr>
              </thead>
              <tbody>
                @forelse ($invoices as $invoice)
                <tr>
                  <td scope="row" style="text-align: center">
                    {{ $invoices->firstItem() + $loop->index }}
                  </td>
                  <td scope="row">
                    {{ $invoice->invoice }}
                  </td>
                  <td scope="row">
                    {{ $invoice->name }}
                  </td>
                  <td scope="row">
                    {{ moneyFormat($invoice->grand_total) }}
                  </td>
                  <td scope="row">
                    {{ $invoice->status }}
                  </td>
                  <td scope="row" class="text-center">
                    <a href="{{ route('admin.orders.show', $invoice->id) }}" class="btn btn-sm btn-primary">
                      <i class="fa fa-list-ul"></i>
                    </a>
                  </td>
                </tr>
                @empty
                <tr>
                  <td scope="row" colspan="6">
                    <div class="text-danger text-center">No data orders.</div>
                  </td>
                </tr>
                @endforelse
              </tbody>
            </table>
            <div style="text-align: center">
              {{ $invoices->links() }}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
