@extends('layouts.app', ['title' => 'Detail Order - GreenK Online Shop'])
@section('content')
<div class="container-fluid mb-5">
  <div class="row">
    <div class="col-md-12">
      <div class="card border-0 shadow">
        {{-- card header --}}
        <div class="card-header">
          <h6 class="m-0 font-weight-bold"><i class="fas fa-shopping-cart mr-2"></i> Detail Order</h6>
        </div>
        {{-- card body --}}
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-hover">
              {{-- no. invoice --}}
              <tr>
                <td style="width: 25%"> No. Invoice </td>
                <td style="width: 1%">:</td>
                <td>{{ $invoice->invoice }}</td>
              </tr>
              {{-- full name --}}
              <tr>
                <td>Full Name</td>
                <td>:</td>
                <td>{{ $invoice->name }}</td>
              </tr>
              {{-- phone number --}}
              <tr>
                <td>Phone Number</td>
                <td>:</td>
                <td>{{ __('0') . $invoice->phone }}</td>
              </tr>
              {{-- courier, service, and cost --}}
              <tr>
                <td>Courier / Service / Cost</td>
                <td>:</td>
                <td>{{ $invoice->courier }} / {{ $invoice->service }} / {{ moneyFormat($invoice->cost_courier) }}</td>
              </tr>
              {{-- address --}}
              <tr>
                <td>Address</td>
                <td>:</td>
                <td>{{ $invoice->address }}, {{ $invoice->cities->name }}, {{ $invoice->provinces->name }}</td>
              </tr>
              {{-- grand total --}}
              <tr>
                <td>Grand Total</td>
                <td>:</td>
                <td>{{ moneyFormat($invoice->grand_total) }}</td>
              </tr>
              <tr>
                <td>Status</td>
                <td>:</td>
                <td>{{ $invoice->status }}</td>
              </tr>
            </table>
          </div>
        </div>
      </div>

      <div class="card border-0 rounded shadow mt-4">
        <div class="card-body">
          <h5><i class="fa fa-shopping-cart"></i> Detail Order</h5>
          <hr>
          {{-- data orders --}}
          @foreach ($invoice->orders()->get() as $product)
          <hr />
          <div class="row mb-4 mb-md-0">
            <div class="col-md-3">
              {{-- product image --}}
              <img src="{{ $product->image }}" style="width: 100%;border-radius: .5rem; width: 220px; height: 170px">
            </div>
            <div class="col-md-6 mt-2 mt-md-0">
              {{-- product name --}}
              <h5 style="font-size: 1rem;"><strong>{{ $product->product_name }}</strong></h5>
              {{-- product qty --}}
              Quantity: {{ $product->qty }}
            </div>
            {{-- product price --}}
            <div class="col-md-3">
                <p class="m-0 font-weight-bold">{{ moneyFormat($product->price) }}</p>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
