@extends('layouts.app', ['title' => 'Add Product - GreenK Online Shop'])
@section('content')
<div class="container-fluid mb-5">
  <div class="row">
    <div class="col-md-12">
      <div class="card border-0 shadow">
        {{-- card header --}}
        <div class="card-header">
          <h6 class="m-0 font-weight-bold"><i class="fas fa-shopping-bag mr-2"></i> Add Product</h6>
        </div>

        {{-- card body --}}
        <div class="card-body">
          <form action="{{ route('admin.products.store') }}" method="POST" enctype="multipart/form-data">
            @include('admin.products.form', ['submit' => 'Save'])
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
