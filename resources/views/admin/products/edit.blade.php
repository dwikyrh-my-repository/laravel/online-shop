@extends('layouts.app', ['title' => 'Edit Product - GreenK Online Shop'])
@section('content')
<div class="container-fluid mb-5">
  <div class="row">
    <div class="col-md-12">
      <div class="card border-0 shadow">
        {{-- card header --}}
        <div class="card-header">
          <h6 class="m-0 font-weight-bold"><i class="fas fa-shopping-bag mr-2"></i> Edit Product</h6>
        </div>

        {{-- card body --}}
        <div class="card-body">
          <form action="{{ route('admin.products.update', $product) }}" method="POST" enctype="multipart/form-data">
            @method('PUT')
            @include('admin.products.form', ['submit' => 'Update'])
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
