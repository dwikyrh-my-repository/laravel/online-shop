@csrf
{{-- image --}}
<div class="form-group">
  <label for="image" class="form-label">Image</label>
  <input type="file" name="image" class="form-control @error('image') is-invalid @enderror" id="image" accept="image/*">
  @error('image')
  <div class="invalid-feedback">
    <strong>{{ $message }}</strong>
  </div>
  @enderror
</div>

{{-- product name --}}
<div class="form-group">
  <label for="title">Product Name</label>
  <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" id="title" value="{{ old('title', $product->title) }}" placeholder="Product Name">
  @error('title')
  <div class="invalid-feedback">
    <strong> {{ $message }}</strong>
  </div>
  @enderror
</div>

<div class="row">
  {{-- category --}}
  <div class="col-md-6">
    <div class="form-group">
      <label for="category" class="form-label">Category</label>
      <select name="category" class="form-control @error('category') is-invalid @enderror" id="category">
        <option disabled>Choose Category</option>
        @foreach ($categories as $category)
        <option {{ old('category') == $category->id || $product?->category_id == $category->id ? 'selected' : '' }} value="{{ $category->id }}">
          {{ $category->name }}
        </option>
        @endforeach
      </select>

      @error('category')
      <div class="invalid-feedback">
        <strong>{{ $message }}</strong>
      </div>
      @enderror
    </div>
  </div>

  {{-- weight --}}
  <div class="col-md-6">
    <div class="form-group">
      <label for="weight" class="form-label">Weight (gram)</label>
      <input type="number" name="weight" class="form-control @error('weight') is-invalid @enderror" id="weight" value="{{ old('weight', $product->weight) }}" placeholder="Product Weight (gram)">
      @error('weight')
      <div class="invalid-feedback">
        <strong>{{ $message }}</strong>
      </div>
      @enderror
    </div>
  </div>
</div>

{{-- description --}}
<div class="form-group">
  <label for="content" class="form-label">Description</label>
  <textarea name="content" class="form-control content @error('content') is-invalid @enderror" id="content" rows="10" placeholder="Product Description">{{ old('content', $product->content) }}</textarea>
  @error('content')
  <div class="invalid-feedback">
    <strong>{{ $message }}</strong>
  </div>
  @enderror
</div>

<div class="row">
  {{-- price --}}
  <div class="col-md-6">
    <div class="form-group">
      <label for="price" class="form-label">Price</label>
      <input type="number" name="price" class="form-control @error('price') is-invalid @enderror" id="price" value="{{ old('price', $product->price) }}"
      placeholder="Product Price (Rp)">
      @error('price')
      <div class="invalid-feedback">
        <strong>{{ $message }}</strong>
      </div>
      @enderror
    </div>
  </div>

  {{-- discount --}}
  <div class="col-md-6">
    <div class="form-group">
      <label for="discount" class="form-label">Discount (%)</label>
      <input type="number" name="discount" class="form-control @error('discount') is-invalid @enderror" id="discount" value="{{ old('discount', $product->discount) }}"
      placeholder="Discount (%)">
      @error('discount')
      <div class="invalid-feedback">
        <strong>{{ $message }}</strong>
      </div>
      @enderror
    </div>
  </div>
</div>

{{-- button submit --}}
<button class="btn btn-primary mr-1 btn-submit" type="submit">
  {{ $submit }}
</button>

<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.10.4/tinymce.min.js"></script>
<script>
  var editor_config = {
    selector: "textarea.content"
    , plugins: [
      "advlist autolink lists charmap print preview hr anchor pagebreak"
      , "searchreplace wordcount visualblocks visualchars code fullscreen"
      , "insertdatetime nonbreaking save table contextmenu directionality"
      , "emoticons template paste textcolor colorpicker textpattern"
    ]
    , toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media"
    , relative_urls: false
  , };

  tinymce.init(editor_config);
</script>
