@extends('layouts.app', ['title' => 'Products - GreenK Online Shop'])

@section('content')
<div class="container-fluid mb-5">
  <div class="row">
    <div class="col-md-12">
      <div class="card border-0 shadow">
        {{-- card header --}}
        <div class="card-header">
          <h6 class="m-0 font-weight-bold"><i class="fa fa-shopping-bag mr-2"></i>
            Prodcuts
          </h6>
        </div>

        {{-- card-body --}}
        <div class="card-body">
          <form action="{{ route('admin.products.index') }}" method="GET">
            <div class="row">
              <div class="col-md-4 mb-3 mb-md-0">
                <a href="{{ route('admin.products.create') }}" class="btn btn-primary btn-md">
                  Add Product
                </a>
              </div>

              {{-- search products--}}
              <div class="col-md-8">
                <div class="form-group">
                  <div class="input-group">
                    <input type="search" class="form-control form-control-md" name="q" placeholder="Search Products...">
                    <div class="input-group-append">
                      <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </form>
          {{-- table of products --}}
          <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th scope="col" style="text-align: center;width: 6%">No.</th>
                  <th scope="col">Product Name</th>
                  <th scope="col">Image</th>
                  <th scope="col">Price</th>
                  <th scope="col">Discount</th>
                  <th scope="col">Category</th>
                  <th scope="col" style="width: 15%;text-align: center">Action</th>
                </tr>
              </thead>
              <tbody>
                @forelse ($products as $product)
                <tr>
                  <td scope="row" class="text-center">
                    {{ $products->firstItem() + $loop->index }}
                  </td>
                  <td scope="row">
                    {{ $product->title }}
                  </td>
                  <td scope="row">
                    <img src="{{ asset($product->image) }}" style="width: 70px; height: 60px">
                  </td>
                  <td scope="row" style="width: 155px">
                    {{ moneyFormat($product->price) }}
                  </td>
                  <td scope="row">
                    {{ $product->discount }}%
                  </td>
                  <td scope="row">
                    {{ $product->category?->name }}
                  </td>
                  <td scope="row" class="text-center">
                    <a href="{{ route('admin.products.edit', $product) }}" class="btn btn-primary btn-sm mb-2 mb-md-0">
                      <i class="fa fa-pencil-alt"></i>
                    </a>
                    <button onclick="Delete(this.id)" class="btn btn-sm btn-danger" id="{{ $product->slug }}">
                      <i class="fa fa-trash"></i>
                    </button>
                  </td>
                </tr>
                @empty
                <tr>
                  <td scope="row" colspan="7">
                    <div class="text-danger text-center">No data products.</div>
                  </td>
                </tr>
                @endforelse
              </tbody>
            </table>
            <div style="text-align: center">
              {{ $products->links() }}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
    /* ajax delete */
    function Delete(id) {
      var id = id;
      var token = $("meta[name='csrf-token']").attr("content");

      Swal.fire({
        title: "Want to delete this data?"
        , text: "Data that has been deleted cannot be recovered"
        , icon: "warning"
        , showCancelButton: true
        , confirmButtonColor: '#3085d6'
        , cancelButtonColor: '#d33'
        , confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          jQuery.ajax({
            url: "/admin/products/" + id
            , data: {
              "id": id
              , "_token": token
            }
            , type: "DELETE"
          });
          Swal.fire({
            position: 'center'
            , icon: 'success'
            , title: 'Data product has been deleted'
            , showConfirmButton: false
            , timer: 1500
          , }).then(function() {
            location.reload();
          });
        }
      });
    }
</script>
@endsection
