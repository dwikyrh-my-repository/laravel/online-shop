@extends('layouts.app', ['title' => 'Profile - GreenK Online Shop'])
@section('content')
<div class="container-fluid">
  <div class="row">
    {{-- session (feedback) --}}
    <div class="col-md-12">
      @if(session('status'))
      <div class="alert alert-success alert-dismissible fade show" role="alert"">
        @if(session('status') == 'profile-information-updated')
        Profile has been updated.
        @elseif (session('status') == 'password-updated')
        Password has been updated.
        @elseif (session('status') == 'two-factor-authentication-disabled')
        Two factor authentication disabled.
        @elseif (session('status') == 'two-factor-authentication-enabled')
        Two factor authentication enabled.
        @elseif (session('status') == 'recovery-codes-generated')
        Recovery codes generated.
        @endif
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @endif
    </div>
  </div>
  {{-- 2fa --}}
  <div class="row">
    @if(Laravel\Fortify\Features::enabled(Laravel\Fortify\Features::twoFactorAuthentication()))
    <div class="col-md-5 mb-5">
        <div class="card border-0 shadow">
            <div class="card-header">
                <h6 class="m-0 font-weight-bold"><i class="fas fa-key mr-2"></i> Two-Factor Authentication</h6>
            </div>
            <div class="card-body">
                {{-- enable 2fa --}}
                @if(!auth()->user()->two_factor_secret)
                <form action="{{ url('user/two-factor-authentication')}}" method="POST">
                    @csrf
                    <button type="submit" class="btn btn-primary">
                        Enable Two-Factor
                    </button>
                </form>
                @else
                {{-- disable 2fa --}}
                <form action="{{ url('user/two-factor-authentication') }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger mb-2">
                        Disable Two-Factor
                    </button>
                </form>

                {{-- show svg qr code, after enabling 2fa --}}
                <p>
                    Two-factor authentication is now enabled. Scan the following QR code using your phone's
                    authenticator app.
                </p>
                <div class="mb-3">
                    {!! auth()->user()->twoFactorQrCodeSvg() !!}
                </div>

                {{-- show 2fa recovery 2fa --}}
                <p>
                    Keep this recovery code safe. This can be used to restore access to your account if your
                    two-factor authentication device is lost.
                </p>
                <div style="background-color: rgb(44, 44, 44); color: #fff" class="rounded p-3 mb-2">
                    @foreach (json_decode(decrypt(auth()->user()->two_factor_recovery_codes), true) as $code)
                    <div>{{ $code }}</div>
                    @endforeach
                </div>

                {{-- regenerate 2fa recovery codes --}}
                <form method="POST" action="{{ url('user/two-factor-recovery-codes') }}">
                    @csrf
                    <button type="submit" class="btn btn-dark">
                        Regenerate Recovery Codes
                    </button>
                </form>
                @endif
            </div>
        </div>
    </div>
    @endif

    {{-- edit profile --}}
    <div class="col-md-7">
      <div class="card border-0 shadow">
        {{-- card header --}}
        <div class="card-header">
          <h6 class="m-0 font-weight-bold"><i class="fas fa-user-circle mr-2"></i> Edit Profile</h6>
        </div>
        {{-- card body --}}
        <div class="card-body">
          <form action="{{ route('user-profile-information.update') }}" method="POST">
            @csrf
            @method('PUT')

            {{-- name --}}
            <div class="form-group">
              <label for="name" class="form-label">Name</label>
              <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name"
              value="{{ old('name', auth()->user()->name) }}" placeholder="Full Name" autocomplete="name">
              @error('name')
              <div class="invalid-feedback">
                <strong>{{ $message }}</strong>
              </div>
              @enderror
            </div>

            {{-- email --}}
            <div class="form-group">
              <label for="email" class="form-label">Email</label>
              <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email"
              value="{{ old('email', auth()->user()->email) }}" placeholder="Email" autocomplete="email">
              @error('email')
              <div class="invalid-feedback">
                <strong>{{ $message }}</strong>
              </div>
              @enderror
            </div>
            {{-- button submit --}}
            <button class="btn btn-primary" type="submit">
              Update Profile
            </button>
          </form>
        </div>
      </div>

      {{-- update password --}}
      <div class="card border-0 shadow mt-3 mb-4">
        {{-- card header --}}
        <div class="card-header">
          <h6 class="m-0 font-weight-bold"><i class="fas fa-unlock mr-2"></i> Update Password</h6>
        </div>
        {{-- card body --}}
        <div class="card-body">
          <form method="POST" action="{{ route('user-password.update') }}">
            @csrf
            @method('PUT')
            {{-- current password --}}
            <div class="form-group">
              <label for="current_password" class="form-label">Current Password</label>
              <input type="password" name="current_password" class="form-control @error('current_password') is-invalid @enderror"
              id="current_password" autocomplete="current-password" placeholder="Current Password">
              @error('current_password')
              <div class="invalid-feedback">
                <strong>{{ $message }}</strong>
              </div>
              @enderror
            </div>
            {{-- new password --}}
            <div class="form-group">
              <label for="new-password" class="form-label">New Password</label>
              <input type="password" name="password" class="form-control @error('password') is-invalid @enderror"
              id="new-password" autocomplete="new-password" placeholder="New Password">
              @error('password')
              <div class="invalid-feedback">
                <strong>{{ $message }}</strong>
              </div>
              @enderror
            </div>
            {{-- confirm password --}}
            <div class="form-group">
              <label for="confirm-password" class="form-label">Confirm New Password</label>
              <input type="password" name="password_confirmation" class="form-control" id="confirm-password" autocomplete="new-password"
              placeholder="Confirm New Password">
            </div>
            {{-- button submit --}}
            <button class="btn btn-primary" type="submit">
                Update Password
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
