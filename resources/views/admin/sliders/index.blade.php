@extends('layouts.app', ['title' => 'Sliders - GreenK Online Shop'])
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card border-0 shadow">
        {{-- card header --}}
        <div class="card-header">
          <h6 class="m-0 font-weight-bold"><i class="fas fa-image mr-2"></i> Upload Slider</h6>
        </div>
        {{-- card body --}}
        <div class="card-body">
          {{-- form create slider --}}
          <form action="{{ route('admin.sliders.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            {{-- image --}}
            <div class="form-group">
              <label for="image" class="form-label">Image</label>
              <input type="file" name="image" class="form-control @error('image') is-invalid @enderror" id="image">
              @error('image')
              <div class="invalid-feedback">
                <strong>{{ $message }}</strong>
              </div>
              @enderror
            </div>

            {{-- link --}}
            <div class="form-group">
              <label for="link" class="form-label">Link</label>
              <input type="text" name="link" class="form-control @error('link') is-invalid @enderror" id="link" value="{{ old('link') }}" placeholder="Link">
              @error('link')
              <div class="invalid-feedback">
                <strong>{{ $message }}</strong>
              </div>
              @enderror
            </div>
            {{-- button submit --}}
            <button class="btn btn-primary mr-1 btn-submit" type="submit">
              Save
            </button>
          </form>
        </div>
      </div>

      <div class="card border-0 shadow mt-3 mb-4">
        {{-- card header --}}
        <div class="card-header">
          <h6 class="m-0 font-weight-bold"><i class="fas fa-laptop mr-2"></i> Sliders</h6>
        </div>

        {{-- card body --}}
        <div class="card-body">
          <div class="table-responsive">
            {{-- table of sliders --}}
            <table class="table table-hover">
              <thead>
                <tr>
                  <th scope="col" style="text-align: center;width: 6%">No.</th>
                  <th scope="col">Image</th>
                  <th scope="col">Link</th>
                  <th scope="col" style="width: 15%;text-align: center">Action</th>
                </tr>
              </thead>
              <tbody>
                @forelse ($sliders as $slider)
                <tr>
                  <td scope="row" class="text-center">
                    {{ $sliders->firstItem() + $loop->index }}
                  </td>
                  <td scope="row" class="text-center">
                    <img src="{{ asset($slider->image) }}" class="rounded img-fluid" style="width:200px; height: 100px">
                  </td>
                  <td scope="row">
                    {{ $slider->link }}
                  </td>
                  <td scope="row" class="text-center">
                    <button onclick="Delete(this.id)" class="btn btn-sm btn-danger" id="{{ $slider->id }}">
                      <i class="fa fa-trash"></i>
                    </button>
                  </td>
                </tr>
                @empty
                <tr>
                  <td scope="row" colspan="4">
                    <div class="text-danger text-center">No data sliders.</div>
                  </td>
                </tr>
                @endforelse
              </tbody>
            </table>
            {{-- pagination --}}
            <div style="text-align: center">
              {{ $sliders->links() }}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  /* ajax delete */
  function Delete(id) {
    var id = id;
    var token = $("meta[name='csrf-token']").attr("content");

    Swal.fire({
      title: "Want to delete this data?"
      , text: "Data that has been deleted cannot be recovered"
      , icon: "warning"
      , showCancelButton: true
      , confirmButtonColor: '#3085d6'
      , cancelButtonColor: '#d33'
      , confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        jQuery.ajax({
          url: "/admin/sliders/" + id
          , data: {
            "id": id
            , "_token": token
          }
          , type: "DELETE"
        });
        Swal.fire({
          position: 'center'
          , icon: 'success'
          , title: 'Data slider has been deleted'
          , showConfirmButton: false
          , timer: 1500
        , }).then(function() {
          location.reload();
        });
      }
    });
  }
</script>
@endsection
