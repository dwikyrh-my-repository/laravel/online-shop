@extends('layouts.app', ['title' => 'Edit User - GreenK Online Shop'])
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card border-0 shadow">
        {{-- card header --}}
        <div class="card-header">
          <h6 class="m-0 font-weight-bold"><i class="fas fa-user-circle mr-2"></i> Edit User</h6>
        </div>
        {{-- card body --}}
        <div class="card-body">
          {{-- form add user --}}
          <form action="{{ route('admin.users.update', $user) }}" method="POST" enctype="multipart/form-data">
            @method('PUT')
            @include('admin.users.form', ['submit' => 'Update'])
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
