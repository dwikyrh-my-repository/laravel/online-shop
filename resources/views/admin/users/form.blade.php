@csrf

<div class="row">
  {{-- full name --}}
  <div class="col-md-6">
    <div class="form-group">
      <label for="name" class="form-label">Full Name</label>
      <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name" value="{{ old('name', $user->name) }}" placeholder="Full Name">
      @error('name')
      <div class="invalid-feedback">
        <strong> {{ $message }}</strong>
      </div>
      @enderror
    </div>
  </div>
  {{-- email address --}}
  <div class="col-md-6">
    <div class="form-group">
      <label for="email" class="form-label">Email Address</label>
      <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" value="{{ old('email', $user->email) }}" placeholder="Email Address">
      @error('email')
      <div class="invalid-feedback">
        <strong> {{ $message }}</strong>
      </div>
      @enderror
    </div>
  </div>
</div>

<div class="row">
  {{-- password --}}
  <div class="col-md-6">
    <div class="form-group">
      <label for="password" class="form-label">Password</label>
      <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="Password">
      @error('password')
      <div class="invalid-feedback">
        <strong> {{ $message }}</strong>
      </div>
      @enderror
    </div>
  </div>
  
  {{-- confirm password --}}
  <div class="col-md-6">
    <div class="form-group">
      <label for="password-confirm" class="form-label">Password Confirm</label>
      <input type="password" name="password_confirmation" class="form-control" placeholder="Password Confirm">
    </div>
  </div>
</div>
{{-- button submit --}}
<button class="btn btn-primary mr-1 btn-submit" type="submit">
  {{ $submit }}
</button>
