@extends('layouts.auth', ['title' => 'Confirm Password - GreenK Online Shop'])
@section('content')
<div class="container mt-5">
  <div class="row justify-content-center">
    <div class="col-md-6 col-lg-4">
      <div class="card o-hidden border-0 shadow-lg mb-3 mt-5">
        <div class="card-body p-4">
          {{-- session (feedback) --}}
          @if (session('status'))
          <div class="alert alert-success alert-dismissible">
            {{ session('status') }}
          </div>
          @endif
          {{-- text header --}}
          <div class="text-center">
            <h1 class="h5 text-gray-900 mb-3">Confirm Password</h1>
          </div>

          {{-- form confirm password --}}
          <form method="POST" action="{{ route('password.confirm') }}">
            @csrf

            {{-- password --}}
            <div class="form-group">
              <label for="password" class="form-label">Password</label>
              <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="password" tabindex="1" />
              @error('password')
              <div class="invalid-feedback">
                <strong>{{ $message }}</strong>
              </div>
              @enderror
            </div>

            {{-- button submit --}}
            <button type="submit" class="btn btn-primary btn-md btn-block" tabindex="4">
              Confirm Password
            </button>
          </form>
        </div>
      </div>
      <div class="text-center text-white">
      </div>
    </div>

  </div>

</div>
@endsection
