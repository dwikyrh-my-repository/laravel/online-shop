@extends('layouts.auth', ['title' => 'Forgot Password - GreenK Online Shop'])
@section('content')
<div class="container mt-5">
  <div class="row justify-content-center">
    <div class="col-md-6 col-lg-4">
      <div class="card o-hidden border-0 shadow-lg mb-3 mt-5">
        <div class="card-body p-4">
          {{-- feedback (session) --}}
          @if(session('status'))
          <div class="alert alert-success" role="alert">
            {{ session('status') }}
          </div>
          @endif
          {{-- reset password text --}}
          <div class="text-center">
            <h1 class="h5 text-gray-900 mb-3">Reset Password</h1>
          </div>
          <form action="{{ route('password.email') }}" method="POST">
            @csrf
            {{-- email address --}}
            <div class="form-group">
              <label for="email" class="font-weight-bold">Email Address</label>
              <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" value="{{ old('email') }}"
              autocomplete="email">
              @error('email')
              <div class="invalid-feedback">
                <strong>{{ $message }}</strong>
              </div>
              @enderror
            </div>
            {{-- button submit --}}
            <button type="submit" class="btn btn-primary btn-block">Send Password Reset Link</button>
            <hr>
            {{-- link view login --}}
            <a href="{{ route('login') }}">Back to Login</a>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
