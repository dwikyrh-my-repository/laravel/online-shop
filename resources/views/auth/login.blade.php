@extends('layouts.auth', ['title' => 'Login - GreenK Online Shop'])
@section('content')
<div class="container mt-5">
  <div class="row justify-content-center">
    <div class="col-md-6 col-lg-4">
      <div class="card o-hidden border-0 shadow-lg mb-3 mt-5">
        <div class="card-body p-4">
          {{-- feedback (session) --}}
          @if (session('status'))
          <div class="alert alert-success" role="alert">
            {{ session('status') }}
          </div>
          @endif
          {{-- login text --}}
          <div class="text-center">
            <h1 class="h5 text-gray-900 mb-3">Login Admin</h1>
          </div>
          {{-- form login --}}
          <form action="{{ route('login') }}" method="POST">
            @csrf
            {{-- email address --}}
            <div class="form-group">
              <label for="email" class="font-weight-bold">Email Address</label>
              <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" value="{{ old('email') }}" autocomplete="email" />
              @error('email')
              <div class="invalid-feedback">
                <strong>{{ $message }}</strong>
              </div>
              @enderror
            </div>
            {{-- password --}}
            <div class="form-group">
              <label for="password" class="font-weight-bold">Password</label>
              <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="password" />
              @error('password')
              <div class="invalid-feedback">
                <strong>{{ $message }}</strong>
              </div>
              @enderror
            </div>
            {{-- button submit --}}
            <button type="submit" class="btn btn-primary btn-block">Login</button>
            <hr>
            {{-- link view forgot password --}}
            <a href="{{ route('password.request') }}">Forgot Password?</a>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
