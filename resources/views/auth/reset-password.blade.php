@extends('layouts.auth', ['title' => 'Reset Password - GreenK Online Shop'])
@section('content')
<div class="container mt-5">
  <div class="row justify-content-center">
    <div class="col-md-6 col-lg-4">
      <div class="card o-hidden border-0 shadow-lg mb-3 mt-5">
        <div class="card-body p-4">
          {{-- feedback (session) --}}
          @if(session('status'))
          <div class="alert alert-success" role="alert">
            {{ session('status') }}
          </div>
          @endif
          {{-- reset password text --}}
          <div class="text-center">
            <h1 class="h5 text-gray-900 mb-3">Update Password</h1>
          </div>
          <form action="{{ route('password.update') }}" method="POST">
            @csrf
            {{-- token --}}
            <input type="hidden" name="token" value="{{ $request->route('token') }}">

            {{-- email address --}}
            <div class="form-group">
              <label for="email" class="font-weight-bold">Email Address</label>
              <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" value="{{ old('email') }}"
              autocomplete="email" />
              @error('email')
              <div class="invalid-feedback">
                <strong>{{ $message }}</strong>
              </div>
              @enderror
            </div>

            {{-- new password --}}
            <div class="form-group">
              <label for="password" class="font-weight-bold">New Password</label>
              <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="password"
              autocomplete="new-password" />
              @error('password')
              <div class="invalid-feedback">
                <strong>{{ $message }}</strong>
              </div>
              @enderror
            </div>

            {{-- password confirm --}}
            <div class="form-group">
                <label for="password-confirm" class="font-weight-bold">Password Confirm</label>
                <input type="password" name="password_confirmation" class="form-control" id="password-confirm" autocomplete="new-password" />
            </div>
            {{-- button submit --}}
            <button type="submit" class="btn btn-primary btn-block">Update Password</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
