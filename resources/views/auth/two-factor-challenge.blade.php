@extends('layouts.auth', ['title' => 'Two Factor Challange - GreenK Online Shop'])
@section('content')
<div class="container mt-5">
  <div class="row justify-content-center">
    <div class="col-md-6 col-lg-4">
      <div class="card o-hidden border-0 shadow-lg mb-3 mt-5">
        <div class="card-body p-4">
          {{-- session(feedback) --}}
          @if (session('status'))
          <div class="alert alert-success alert-dismissible">
            {{ session('status') }}
          </div>
          @endif
          {{-- text header --}}
          <div class="text-center">
            <h1 class="h5 text-gray-900 mb-3">Two Factor Challange </h1>
          </div>

          {{-- form 2fa --}}
          <form method="POST" action="{{ url('/two-factor-challenge') }}">
            @csrf

            {{-- code --}}
            <details class="mb-3">
              <summary>Code</summary>
              <div class="form-group">
                <input type="text" name="code" class="form-control" id="code">
              </div>
            </details>

            {{-- recovery code --}}
            <details class="mb-3">
              <summary>Recovery Code</summary>
              <div class="form-group">
                <input type="text" name="recovery_code" class="form-control" id="recovery_code">
              </div>
            </details>
            {{-- button submit --}}
            <button type="submit" class="btn btn-primary btn-md btn-block" tabindex="4">
              Login
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
