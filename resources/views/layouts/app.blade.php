<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="description" content="GreenK Online Shop is an online store that is in great demand by Indonesian people.">
  <meta name="author" content="GreenK">
  <meta name="keywords" content="Online Shop, Shopping, Product">
  {{-- csrf token --}}
  <meta name="csrf-token" content="{{ csrf_token() }}">
  {{-- favicon --}}
  <link rel="shortcut icon" href="{{ asset('assets/img/logo-greenk.jpg') }}" type="image/jpeg">
  {{-- custom fonts for this template --}}
  <link href="{{ asset('assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  {{-- custom styles for this template --}}
  <link href="{{ asset('assets/css/sb-admin-2.min.css') }}" rel="stylesheet">
  <style>
    .form-control:focus {
      color: #6e707e;
      background-color: #fff;
      border-color: #375dce;
      outline: 0;
      box-shadow: none
    }

    .form-group label {
      font-weight: bold
    }

    #wrapper #content-wrapper {
      background-color: #e2e8f0;
      width: 100%;
      overflow-x: hidden;
    }

    .card-header {
      padding: .75rem 1.25rem;
      margin-bottom: 0;
      background-color: #4e73de;
      border-bottom: 1px solid #e3e6f0;
      color: white;
    }

  </style>
  {{-- jQuery --}}
  <script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
  {{-- sweet alert --}}
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <title>{{ $title ?? config('app.name') }}</title>
</head>

<body id="page-top">
  <div id="wrapper">
    {{-- sidebar --}}
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      {{-- sidebar brand --}}
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('admin.dashboard.index') }}">
        <div class="sidebar-brand-icon">
            <img src="{{  asset('assets/img/logo-greenk.jpg') }}" alt="" style="width: 42px" class="rounded">
        </div>
        <div class="sidebar-brand-text mx-3">GreenK</div>
      </a>
      <hr class="sidebar-divider my-0">

      {{-- nav item dashboard --}}
      <li class="nav-item {{ request()->is('admin/dashboard*') ? ' active' :  '' }}">
        <a class="nav-link" href="{{ route('admin.dashboard.index') }}">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>
      <hr class="sidebar-divider">

      {{-- nav item - pages collapse menu --}}
      <li class="nav-item {{ request()->routeIs('admin.categories.index') ? ' active' :  '' }} {{ request()->routeIs('admin.products.index') ? ' active' :  '' }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fa fa-shopping-bag"></i>
          <span>Products</span>
        </a>
        <div id="collapseTwo" class="collapse {{ request()->routeIs('admin.categories*') ? ' show' :  '' }} {{ request()->routeIs('admin.products*') ? ' show' :  '' }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Categories & Products</h6>
            <a class="collapse-item {{ request()->routeIs('admin.categories*') ? ' active' : '' }}" href="{{ route('admin.categories.index') }}">Categories</a>
            <a class="collapse-item {{ request()->routeIs('admin.products*') ? ' active' : '' }}" href="{{ route('admin.products.index') }}">Products</a>
          </div>
        </div>
      </li>

      {{-- orders --}}
      <li class="nav-item {{ request()->routeIs('admin.orders*') ? ' active' :  '' }}">
        <a class="nav-link" href="{{ route('admin.orders.index') }}">
          <i class="fas fa-shopping-cart"></i>
          <span>Orders</span></a>
      </li>

      {{-- customers --}}
      <li class="nav-item {{ request()->routeIs('admin.customers*') ? ' active' :  '' }}">
        <a class="nav-link" href="{{ route('admin.customers.index') }}">
          <i class="fas fa-users"></i>
          <span>Customers</span></a>
      </li>

      {{-- sliders --}}
      <li class="nav-item {{ request()->routeIs('admin/sliders*') ? ' active' :  '' }}">
        <a class="nav-link" href="{{ route('admin.sliders.index') }}">
          <i class="fas fa-laptop"></i>
          <span>Sliders</span></a>
      </li>

      {{-- profile --}}
      <li class="nav-item {{ request()->routeIs('admin.profile*') ? ' active' :  '' }}">
        <a class="nav-link" href="{{ route('admin.profile.index') }}">
          <i class="fas fa-user-circle"></i>
          <span>Profile</span></a>
      </li>

      <li class="nav-item {{ request()->routeIs('admin.users*') ? ' active' :  '' }}">
        <a class="nav-link" href="{{ route('admin.users.index') }}">
          <i class="fas fa-users"></i>
          <span>Users</span></a>
      </li>
      <hr class="sidebar-divider d-none d-md-block">

      {{-- sidebar toggle --}}
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>
    </ul>

    {{-- content wrapper --}}
    <div id="content-wrapper" class="d-flex flex-column">
      {{-- main content --}}
      <div id="content">
        {{-- topbar --}}
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          {{-- sidebar toggle (topbar) --}}
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          {{-- topbar navbar --}}
          <ul class="navbar-nav ml-auto">

            <div class="topbar-divider d-none d-sm-block"></div>

            {{-- nav item - user information --}}
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ auth()->user()?->name }}</span>
                <img class="img-profile rounded-circle" src="{{ auth()->user()?->avatar_url }}">
              </a>
              {{-- dropdown - user information --}}
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>
          </ul>
        </nav>
        {{-- content --}}
        <main>
          @yield('content')
        </main>
      </div>
      {{-- footer --}}
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span><strong>©2022 GreenK Online Shop.</strong> All Rights Reserved.</span>
          </div>
        </div>
      </footer>
    </div>
  </div>
  {{-- scroll to top button --}}
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  {{-- logout modal --}}
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Are you sure you want to exit?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Please select "Logout" below to end the current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <form method="POST" action="{{ route('logout') }}">
            @csrf
            <a class="btn btn-primary" href="{{ route('logout') }}" onclick="event.preventDefault();
            this.closest('form').submit();">
              Log Out
            </a>
          </form>
        </div>
      </div>
    </div>
  </div>

  {{-- jQuery --}}
  <script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
  {{-- bootstrap core javascript--}}
  <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  {{-- core plugin javascript--}}
  <script src="{{ asset('assets/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

  {{-- custom scripts for all pages--}}
  <script src="{{ asset('assets/js/sb-admin-2.min.js') }}"></script>

  {{-- page level plugins --}}
  {{-- <script src="{{ asset('assets/vendor/chart.js/Chart.min.js') }}"></script> --}}

  {{-- page level custom scripts --}}
  {{-- <script src="{{ asset('assets/js/demo/chart-area-demo.js') }}"></script>
  <script src="{{ asset('assets/js/demo/chart-pie-demo.js') }}"></script> --}}
  <script>
    //sweetalert for success message
    @if(session('success'))
    Swal.fire({
      position: 'center'
      , icon: 'success'
      , title: "{{ session('success') }}"
      , showConfirmButton: false
      , timer: 1700
    });
    @endif
  </script>
  @method('scripts')
</body>
</html>
