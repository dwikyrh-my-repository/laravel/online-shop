<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="description" content="GreenK Online Shop is an online store that is in great demand by Indonesian people.">
  <meta name="author" content="GreenK">
  <meta name="keywords" content="Online Shop, Shopping, Product">
  {{-- favicon --}}
  <link rel="shortcut icon" href="{{ asset('assets/img/logo-greenk.jpg') }}" type="image/jpeg">
  {{-- custom fonts for this template --}}
  <link href="{{ asset('assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  {{-- custom styles for this template --}}
  <link href="{{ asset('assets/css/sb-admin-2.min.css') }}" rel="stylesheet">
  <title>{{ $title ?? config('app.name') }}</title>
</head>

<body style="background-color: #fdfefe">
  @yield('content')
  {{-- bootstrap core javascript --}}
  <script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  {{-- core plugin javascript --}}
  <script src="{{ asset('assets/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
  {{-- custom scripts for all pages --}}
  <script src="{{ asset('assets/js/sb-admin-2.min.js') }}"></script>
</body>

</html>
