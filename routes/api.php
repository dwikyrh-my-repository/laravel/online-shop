<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CartController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\CheckoutController;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\RajaOngkirController;
use App\Http\Controllers\Api\SliderController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


# authentication
Route::post('/login', [AuthController::class, 'login'])->name('api.customer.login');
Route::post('/register', [AuthController::class, 'register'])->name('api.customer.register');
Route::get('/user', [AuthController::class, 'getUser'])->name('api.customer.user');

# orders
Route::get('/orders', [OrderController::class, 'index'])->name('api.orders.index');
Route::get('/orders/{snap_token?}', [OrderController::class, 'show'])->name('api.orders.show');

# categories
Route::get('/categories', [CategoryController::class, 'index'])->name('api.categories.index');
Route::get('/categories/fiveCategories', [CategoryController::class, 'getFiveCategories'])->name('api.categories.getFiveCategories');
Route::get('/categories/{category?}', [CategoryController::class, 'show'])->name('api.categories.show');
Route::get('/categoriesHeader', [CategoryController::class, 'categoryHeader'])->name('api.categories.categoryHeader');

# prodcuts
Route::get('/products', [ProductController::class, 'index'])->name('api.products.index');
Route::get('/products/search', [ProductController::class, 'search'])->name('api.products.search');
Route::get('/products/{product?}', [ProductController::class, 'show'])->name('api.products.show');

# carts
Route::get('/carts', [CartController::class, 'index'])->name('api.carts.index');
Route::post('/carts', [CartController::class, 'store'])->name('api.carts.store');
Route::get('/carts/total', [CartController::class, 'getTotalCart'])->name('api.carts.total');
Route::get('/carts/totalWeight', [CartController::class, 'getTotalWeight'])->name('api.carts.getCartTotalWeight');
Route::post('/carts/remove', [CartController::class, 'removeCart'])->name('api.carts.remove');
Route::post('/carts/removeAll', [CartController::class, 'removeAllCart'])->name('api.carts.removeAll');

# rajaongkir
Route::get('/rajaongkir/provinces', [RajaOngkirController::class, 'getProvinces'])->name('api.rajaongkir.getProvinces');
Route::get('/rajaongkir/cities', [RajaOngkirController::class, 'getCities'])->name('api.rajaongkir.getCities');
Route::post('/rajaongkir/checkOngkir', [RajaOngkirController::class, 'checkOngkir'])->name('api.rajaongkir.checkOngkir');

# checkout
Route::post('/checkout', [CheckoutController::class, 'store'])->name('api.checkout.store');
Route::post('/notificationHandler', [CheckoutController::class, 'notificationHandler'])->name('api.notificationHandler');

# slider
Route::get('/sliders', [SliderController::class, 'index'])->name('api.slider.index');