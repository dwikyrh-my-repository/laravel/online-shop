<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\CustomerController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\SliderController;
use App\Http\Controllers\Admin\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
# admin
Route::controller(DashboardController::class)->group(function() {
    Route::get('/admin/dashboard', 'index')->name('admin.dashboard.index');
});
# admin / categories
Route::resource('/admin/categories', CategoryController::class, ['as' => 'admin']);

# admin / products
Route::resource('/admin/products', ProductController::class, ['as' => 'admin']);

# admin / orders
// Route::resource('/admin/orders', OrderController::class, ['only' => ['index', 'show'], 'as' => 'admin']);
Route::get('/admin/orders', [OrderController::class, 'index'])->name('admin.orders.index');
Route::get('/admin/orders/{invoice}', [OrderController::class, 'show'])->name('admin.orders.show');

# admin / customers
Route::get('/admin/customers', [CustomerController::class, 'index'])->name('admin.customers.index');

# admin / sliders
Route::resource('/admin/sliders', SliderController::class, ['only' => ['index', 'store', 'destroy'], 'as' => 'admin']);

# admin / profile
Route::get('/admin/profile', [ProfileController::class, 'index'])->name('admin.profile.index');

# admin / users
Route::resource('/admin/users', UserController::class, ['as' => 'admin']);